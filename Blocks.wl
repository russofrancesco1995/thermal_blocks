(* ::Package:: *)

(* ::Chapter:: *)
(*Blocks at finite T with \[Mu]*)


(* ::Text:: *)
(*The variables used here are usually (Q,t,y,p) which corresponds to (q,y,z,p) in the paper*)


(* ::Section:: *)
(*Definitions*)


(* ::Subsection::Closed:: *)
(*Casimir from 1802.10537*)


(* ::Text:: *)
(*Here we are using the set of variables (Q,u,s) where s=p^2 and u=y+y^-1-2*)


MH[f_]:=((4-6 s)D[f,s]-4 (-1+s) s D[D[f,s],s])/u-(Q (2+u+Q (-4+Q (2+u))) (\[CapitalDelta]\[Phi] (-2+s \[CapitalDelta]\[Phi]) f+4 (-1+s) ((-1+s+s \[CapitalDelta]\[Phi])D[f,s]+(-1+s) s D[D[f,s],s])))/(2 (1+Q^2-Q (2+u))^2)+(Q (\[CapitalDelta]\[Phi] (1+(-1+s) \[CapitalDelta]\[Phi]) f+2 s ((-1+2 s+2 (-1+s) \[CapitalDelta]\[Phi])D[f,s]+2 (-1+s) s D[D[f,s],s])))/(-1+Q)^2+(2 (3+u+Q (-6+Q (3+u)-u (9+2 u)))D[f,u])/(1+Q^2-Q (2+u))+u (4+u) D[D[f,u],u]+(2 Q (1+2 Q^3-Q^2 (3+u))D[f,Q])/((-1+Q)^3-(-1+Q) Q u)+Q^2 D[D[f,Q],Q];


MM[\[CapitalDelta]o_,l_][f_]:=MH[f]-\[CapitalDelta]o(\[CapitalDelta]o-3)f-l(l+1)f;


(* ::Subsection::Closed:: *)
(*Laplacian and the weight-shifting operator in (Q, t, y ,p) - variables*)


(* ::Text:: *)
(*Casimir*)


lapQtyp[\[CapitalDelta]\[Phi]_,l_][f_]:=1/(-1+Q)^2 (-2 Q \[CapitalDelta]\[Phi] (1+2 l p Sqrt[1-p^2] y+(-1+p^2) \[CapitalDelta]\[Phi]) f-2 p Q (2 l p Sqrt[1-p^2] y-2 \[CapitalDelta]\[Phi]+p^2 (1+2 \[CapitalDelta]\[Phi])) D[f,p]-2 p^2 (-1+p^2) Q D[f,p,p]+4 p Sqrt[1-p^2] Q (1+y^2) \[CapitalDelta]\[Phi] D[f,y]+4 p^2 Sqrt[1-p^2] Q (1+y^2) D[f,y,p])+1/(p^2 (-1+t)^2) (2 l (1+l) p^2 t f+2 p t (-1+2 p^2+2 l p Sqrt[1-p^2] y) D[f,p]+2 p^2 (-1+p^2) t D[f,p,p]+2 t (y (1-2 p^2+2 p Sqrt[1-p^2] y)-2 l p Sqrt[1-p^2] (1+y^2)) D[f,y]-4 p^2 Sqrt[1-p^2] t (1+y^2) D[f,y,p]-2 t (-1+2 p^2-2 p Sqrt[1-p^2] y) (1+y^2) D[f,y,y])+1/(p^2 (Q-t)^2) (p^2 Q t \[CapitalDelta]\[Phi] (-2-2 l p (-Sqrt[1-p^2] y+Sqrt[1-p^2] Sqrt[1+y^2])+p^2 \[CapitalDelta]\[Phi]) f+p (-1+p^2) Q t (-1-2 l p (-Sqrt[1-p^2] y+Sqrt[1-p^2] Sqrt[1+y^2])+p^2 (1+2 \[CapitalDelta]\[Phi])) D[f,p]+p^2 (-1+p^2)^2 Q t D[f,p,p]-Q t (-2 l p (Sqrt[1-p^2]+Sqrt[1-p^2] y^2-Sqrt[1-p^2] y Sqrt[1+y^2])+2 p Sqrt[1-p^2] y^2 (1+p^2 \[CapitalDelta]\[Phi])+2 p^2 (-Sqrt[1+y^2]+p Sqrt[1-p^2] \[CapitalDelta]\[Phi]+p^2 Sqrt[1+y^2] \[CapitalDelta]\[Phi])-y (-1+2 p^2+2 p Sqrt[1-p^2] Sqrt[1+y^2]+2 p^3 Sqrt[1-p^2] Sqrt[1+y^2] \[CapitalDelta]\[Phi])) D[f,y]-2 p^2 (-1+p^2) Q t (Sqrt[1-p^2]+Sqrt[1-p^2] y^2+p Sqrt[1+y^2]-Sqrt[1-p^2] y Sqrt[1+y^2]) D[f,y,p]+Q t (1+y^2) (-1+2 p^2+2 p (-Sqrt[1-p^2] y+Sqrt[1-p^2] Sqrt[1+y^2])) D[f,y,y])+1/(p^2 (-1+Q t)^2) (p^2 Q t \[CapitalDelta]\[Phi] (-2+2 l p (Sqrt[1-p^2] y+Sqrt[1-p^2] Sqrt[1+y^2])+p^2 \[CapitalDelta]\[Phi]) f+p (-1+p^2) Q t (-1+2 l p (Sqrt[1-p^2] y+Sqrt[1-p^2] Sqrt[1+y^2])+p^2 (1+2 \[CapitalDelta]\[Phi])) D[f,p]+p^2 (-1+p^2)^2 Q t D[f,p,p]-Q t (-2 l p (Sqrt[1-p^2]+Sqrt[1-p^2] y^2+Sqrt[1-p^2] y Sqrt[1+y^2])+2 p Sqrt[1-p^2] y^2 (1+p^2 \[CapitalDelta]\[Phi])+2 p^2 (Sqrt[1+y^2]+p Sqrt[1-p^2] \[CapitalDelta]\[Phi]-p^2 Sqrt[1+y^2] \[CapitalDelta]\[Phi])+y (1-2 p^2+2 p Sqrt[1-p^2] Sqrt[1+y^2]+2 p^3 Sqrt[1-p^2] Sqrt[1+y^2] \[CapitalDelta]\[Phi])) D[f,y]-2 p^2 (-1+p^2) Q t (Sqrt[1-p^2]+Sqrt[1-p^2] y^2-p Sqrt[1+y^2]+Sqrt[1-p^2] y Sqrt[1+y^2]) D[f,y,p]-Q t (1+y^2) (1-2 p^2+2 p (Sqrt[1-p^2] y+Sqrt[1-p^2] Sqrt[1+y^2])) D[f,y,y])-2 t D[f,t]-(2 t (1+t) D[f,t])/(-1+t)-2 t^2 D[f,t,t]-2 Q D[f,Q]-(2 Q (1+Q) D[f,Q])/(-1+Q)+(2 t (Q+t) D[f,t]-2 Q (Q+t) D[f,Q])/(Q-t)+(-2 t (1+Q t) D[f,t]-2 Q (1+Q t) D[f,Q])/(-1+Q t)-2 Q^2 D[f,Q,Q]


(* ::Text:: *)
(*Weight-shifting operator*)


qQtyp[\[CapitalDelta]\[Phi]_,l_][f_]:=(-I (1+Q) (l p y+(-1+p^2) (Sqrt[1-p^2]+p y) \[CapitalDelta]\[Phi]) f-I p (-1+p^2) (1+Q) (Sqrt[1-p^2]+p y) D[f,p]+I p (1+Q) (1+y^2) D[f,y])/(-1+Q)+(I l p^2 (1+t) Sqrt[1+y^2] f+I p (-1+p^2) (1+t) Sqrt[1+y^2] D[f,p]+I (1+t) (-2 p Sqrt[1-p^2] Sqrt[1+y^2]+(1-2 p^2) y Sqrt[1+y^2]) D[f,y])/(p (-1+t))+1/(2 p (-1+Q t)) (I p (1+Q t) (-l Sqrt[1-p^2]+p (p Sqrt[1-p^2]+p^2 y-Sqrt[1+y^2]) \[CapitalDelta]\[Phi]) f+I p (-1+p^2) (1+Q t) (p Sqrt[1-p^2]+p^2 y-Sqrt[1+y^2]) D[f,p]+I (1+Q t) (2 p Sqrt[1-p^2] Sqrt[1+y^2]-y Sqrt[1+y^2]+p^2 (-1-y^2+2 y Sqrt[1+y^2]))D[f,y])+1/(2 p (Q-t)) (I p (Q+t) (-l Sqrt[1-p^2]+p (p Sqrt[1-p^2]+p^2 y+Sqrt[1+y^2]) \[CapitalDelta]\[Phi]) f+I p (-1+p^2) (Q+t) (p Sqrt[1-p^2]+p^2 y+Sqrt[1+y^2]) D[f,p]+I (Q+t) (-2 p Sqrt[1-p^2] Sqrt[1+y^2]+y Sqrt[1+y^2]-p^2 (1+y^2+2 y Sqrt[1+y^2])) D[f,y])-2 I p t Sqrt[1+y^2] D[f,t]+2 I Q (Sqrt[1-p^2]+p y) D[f,Q]


(* ::Subsection::Closed:: *)
(*Conservation*)


(* ::Text:: *)
(*Conservation operator for external*)


dv[f_]:=1/2 u1 (D[f,u2]-D[f,u1]-(u1+u2)D[D[f,u1],u2]-4 u1 (-1+u1^2)D[D[f,u1],u1]-(-1+u1^2) (1+u1 u2)D[D[D[f,u1],u1],u2]-(-1+u1^2)^2 D[D[D[f,u1],u1],u1])
dv[\[CapitalDelta]_,l_][f_]:=1/2 (-((-1+l) l u1^2 \[CapitalDelta] f)-l u1 (-u1 u2+l (-1+u1 u2)) D[f,u2]-u1 (-5+l^2 (-1+u1^2)-2 \[CapitalDelta]+3 u1^2 (2+\[CapitalDelta])+l (3+2 \[CapitalDelta]-u1^2 (5+2 \[CapitalDelta]))) D[f,u1]+(1-2 u1^2-2 (-1+l) u1 u2+(-3+2 l) u1^3 u2) D[f,u1,u2]+((-1+u1^2) (3+u1^2 (-7+2 l-\[CapitalDelta])) (-D[f,u1]+u1 D[f,u1,u1]))/u1-(-1+u1^2) (1+u1 u2) (-D[f,u1,u2]+u1 D[f,u1,u1,u2])-((-1+u1^2)^2 (3 D[f,u1]+u1 (-3 D[f,u1,u1]+u1 D[f,u1,u1,u1])))/u1)


dvQtyp[\[CapitalDelta]\[Phi]_][f_]:=1/2 y (-p Sqrt[1-p^2] \[CapitalDelta]\[Phi] f-p^2 Sqrt[1-p^2]D[f,p]+(-1+\[CapitalDelta]\[Phi] -p^2 \[CapitalDelta]\[Phi] +p Sqrt[1-p^2] y \[CapitalDelta]\[Phi])D[f,y]+p(1-p^2+p Sqrt[1-p^2] y)D[D[f,y],p]+(4 y+4 y^3+p Sqrt[1-p^2] \[CapitalDelta]\[Phi]-y \[CapitalDelta]\[Phi]+p^2 y \[CapitalDelta]\[Phi]+p Sqrt[1-p^2] y^2 \[CapitalDelta]\[Phi]-y^3 \[CapitalDelta]\[Phi]+p^2 y^3 \[CapitalDelta]\[Phi])D[D[f,y],y]+p(p Sqrt[1-p^2]-y+p^2 y+p Sqrt[1-p^2] y^2-y^3+p^2 y^3 )D[D[D[f,y],y],p]+(1+y^2)^2 D[D[D[f,y],y],y])
dvQtyp[\[CapitalDelta]\[Phi]_,l\[Phi]_][f_]:=1/2 l\[Phi] p y (-l\[Phi] Sqrt[1-p^2]-p y+l\[Phi] p y) \[CapitalDelta]\[Phi] f+1/2 l\[Phi] p y (y-p^2 y-l\[Phi] (p Sqrt[1-p^2]+y-p^2 y)) D[f,p]+1/2 y (1-l\[Phi]+l\[Phi]^2+2 y^2-3 l\[Phi] y^2+l\[Phi]^2 y^2+p^2 \[CapitalDelta]\[Phi]-2 l\[Phi] p^2 \[CapitalDelta]\[Phi]+p Sqrt[1-p^2] y \[CapitalDelta]\[Phi]+2 p^2 y^2 \[CapitalDelta]\[Phi]-2 l\[Phi] p^2 y^2 \[CapitalDelta]\[Phi]) D[f,y]+1/2 y (-p+2 l\[Phi] p+p^3-2 l\[Phi] p^3+p^2 Sqrt[1-p^2] y-2 p y^2+2 l\[Phi] p y^2+2 p^3 y^2-2 l\[Phi] p^3 y^2) D[f,y,p]+1/2 y (4 y-2 l\[Phi] y+4 y^3-2 l\[Phi] y^3+p Sqrt[1-p^2] \[CapitalDelta]\[Phi]+p^2 y \[CapitalDelta]\[Phi]+p Sqrt[1-p^2] y^2 \[CapitalDelta]\[Phi]+p^2 y^3 \[CapitalDelta]\[Phi]) D[f,y,y]+1/2 y (p^2 Sqrt[1-p^2]-p y+p^3 y+p^2 Sqrt[1-p^2] y^2-p y^3+p^3 y^3) D[f,y,y,p]+1/2 y (1+2 y^2+y^4) D[f,y,y,y]


(* ::Text:: *)
(*Conservation operator for internal free scalar*)


FF[\[CapitalDelta]_][f_]:=(u1^2 (\[CapitalDelta]^2 f+(u2+2 u2 \[CapitalDelta])D[f,u2]+(-1+u2^2+Csc[t1]^2 Sin[t2]^2)D[D[f,u2],u2]-u1 D[f,u1]-(-1+u1^2)D[D[f,u1],u1]))/(Cos[2 t1]-Cos[2 t2]);
FFQtyp[\[CapitalDelta]\[Phi]_][f_]:=-(1/(1/Q+Q-1/t-t))2 y^2 (\[CapitalDelta]\[Phi]^2 f+(-1+p^2) (1+2 \[CapitalDelta]\[Phi]) (\[CapitalDelta]\[Phi] f+p D[f,p])+((p^2 Q (-1+t)^2-(-1+Q)^2 t) (\[CapitalDelta]\[Phi] (-1-\[CapitalDelta]\[Phi]+p^2 (2+\[CapitalDelta]\[Phi])) f+p ((-2 (1+\[CapitalDelta]\[Phi])+p^2 (3+2 \[CapitalDelta]\[Phi]))D[f,p]+p (-1+p^2)D[D[f,p],p])))/((-1+Q)^2 t)-y D[f,y]-(1+y^2)D[D[f,y],y]);


(* ::Subsection::Closed:: *)
(*With eigenvalues*)


(* ::Text:: *)
(*Adding the eigenvalues and conjugating the differential operators so that we get the same solution space as in 1802.10537*)


ClearAll@lapFin
EV[\[CapitalDelta]o_,lo_][f_]:=-2(\[CapitalDelta]o(\[CapitalDelta]o-3)+lo(lo+1))f(*The factor of -2 is the ration with 1802.10537*)
lapFin[\[CapitalDelta]\[Phi]_,l_,\[CapitalDelta]o_,lo_][f_]:=(lapQtyp[\[CapitalDelta]\[Phi],l][f]-EV[\[CapitalDelta]o,lo][f])/-2


lapFinQ\[CapitalDelta]o[\[CapitalDelta]\[Phi]_,l_,\[CapitalDelta]o_,lo_][f_]:=-((lo+lo^2-3 \[CapitalDelta]o) f)+1/(-1+Q)^2 Q (\[CapitalDelta]\[Phi] (1+2 l p Sqrt[1-p^2] y+(-1+p^2) \[CapitalDelta]\[Phi]) f+p ((2 l p Sqrt[1-p^2] y-2 \[CapitalDelta]\[Phi]+p^2 (1+2 \[CapitalDelta]\[Phi])) D[f,p]+p (-1+p^2) D[f,p,p]-2 Sqrt[1-p^2] (1+y^2) (\[CapitalDelta]\[Phi] D[f,y]+p D[f,y,p])))-1/(p^2 (-1+t)^2) t (l (1+l) p^2 f+p (-1+2 p^2+2 l p Sqrt[1-p^2] y) D[f,p]+p^2 (-1+p^2) D[f,p,p]+(y (1-2 p^2+2 p Sqrt[1-p^2] y)-2 l p Sqrt[1-p^2] (1+y^2)) D[f,y]-2 p^2 Sqrt[1-p^2] (1+y^2) D[f,y,p]-(-1+2 p^2-2 p Sqrt[1-p^2] y) (1+y^2) D[f,y,y])-1/(2 p^2 (Q-t)^2) Q t (p^2 \[CapitalDelta]\[Phi] (-2+2 l p Sqrt[1-p^2] (y-Sqrt[1+y^2])+p^2 \[CapitalDelta]\[Phi]) f+p (-1+p^2) (-1+2 l p Sqrt[1-p^2] (y-Sqrt[1+y^2])+p^2 (1+2 \[CapitalDelta]\[Phi])) D[f,p]+p^2 (-1+p^2)^2 D[f,p,p]-(-2 l p Sqrt[1-p^2] (1+y^2-y Sqrt[1+y^2])+2 p Sqrt[1-p^2] y^2 (1+p^2 \[CapitalDelta]\[Phi])+2 p^2 (-Sqrt[1+y^2]+p Sqrt[1-p^2] \[CapitalDelta]\[Phi]+p^2 Sqrt[1+y^2] \[CapitalDelta]\[Phi])-y (-1+2 p^2+2 p Sqrt[1-p^2] Sqrt[1+y^2]+2 p^3 Sqrt[1-p^2] Sqrt[1+y^2] \[CapitalDelta]\[Phi])) D[f,y]-2 p^2 (-1+p^2) (Sqrt[1-p^2]+Sqrt[1-p^2] y^2+p Sqrt[1+y^2]-Sqrt[1-p^2] y Sqrt[1+y^2]) D[f,y,p]+(1+y^2) (-1+2 p^2+2 p Sqrt[1-p^2] (-y+Sqrt[1+y^2])) D[f,y,y])-1/(2 p^2 (-1+Q t)^2) Q t (p^2 \[CapitalDelta]\[Phi] (-2+2 l p Sqrt[1-p^2] (y+Sqrt[1+y^2])+p^2 \[CapitalDelta]\[Phi]) f+p (-1+p^2) (-1+2 l p Sqrt[1-p^2] (y+Sqrt[1+y^2])+p^2 (1+2 \[CapitalDelta]\[Phi])) D[f,p]+p^2 (-1+p^2)^2 D[f,p,p]-(-2 l p Sqrt[1-p^2] (1+y^2+y Sqrt[1+y^2])+2 p Sqrt[1-p^2] y^2 (1+p^2 \[CapitalDelta]\[Phi])+2 p^2 (Sqrt[1+y^2]+p Sqrt[1-p^2] \[CapitalDelta]\[Phi]-p^2 Sqrt[1+y^2] \[CapitalDelta]\[Phi])+y (1-2 p^2+2 p Sqrt[1-p^2] Sqrt[1+y^2]+2 p^3 Sqrt[1-p^2] Sqrt[1+y^2] \[CapitalDelta]\[Phi])) D[f,y]-2 p^2 (-1+p^2) (Sqrt[1-p^2]+Sqrt[1-p^2] y^2-p Sqrt[1+y^2]+Sqrt[1-p^2] y Sqrt[1+y^2]) D[f,y,p]-(1+y^2) (1-2 p^2+2 p Sqrt[1-p^2] (y+Sqrt[1+y^2])) D[f,y,y])+t D[f,t]+(t (1+t) D[f,t])/(-1+t)+t^2 D[f,t,t]+Q D[f,Q]+((1+Q) (\[CapitalDelta]o f+Q D[f,Q]))/(-1+Q)+((Q+t) (\[CapitalDelta]o f-t D[f,t]+Q D[f,Q]))/(Q-t)+((1+Q t) (\[CapitalDelta]o f+t D[f,t]+Q D[f,Q]))/(-1+Q t)+Q (2 \[CapitalDelta]o D[f,Q]+Q D[f,Q,Q])


qQtypQ\[CapitalDelta]o[\[CapitalDelta]\[Phi]_,l_,\[CapitalDelta]o_][f_]:=-(1/2) I ((2 (1+t) Sqrt[1+y^2] (-l p^2 f+(p-p^3) D[f,p]+(2 p Sqrt[1-p^2]-y+2 p^2 y) D[f,y]))/(p (-1+t))+(2 (1+Q) ((l p y+(-1+p^2) (Sqrt[1-p^2]+p y) \[CapitalDelta]\[Phi]) f+p (-1+p^2) (Sqrt[1-p^2]+p y) D[f,p]-p (1+y^2) D[f,y]))/(-1+Q)-1/(p (-1+Q t)) (1+Q t) (p (-l Sqrt[1-p^2]+p (p Sqrt[1-p^2]+p^2 y-Sqrt[1+y^2]) \[CapitalDelta]\[Phi]) f+p (-1+p^2) (p Sqrt[1-p^2]+p^2 y-Sqrt[1+y^2])D[f,p]+(2 p Sqrt[1-p^2] Sqrt[1+y^2]-y Sqrt[1+y^2]+p^2 (-1-y^2+2 y Sqrt[1+y^2])) D[f,y])-1/(p (Q-t)) (Q+t) (p (-l Sqrt[1-p^2]+p (p Sqrt[1-p^2]+p^2 y+Sqrt[1+y^2]) \[CapitalDelta]\[Phi]) f+p (-1+p^2) (p Sqrt[1-p^2]+p^2 y+Sqrt[1+y^2]) D[f,p]-(2 p Sqrt[1-p^2] Sqrt[1+y^2]-y Sqrt[1+y^2]+p^2 (1+y^2+2 y Sqrt[1+y^2])) D[f,y])+4 p t Sqrt[1+y^2] D[f,t]-4 (Sqrt[1-p^2]+p y) (\[CapitalDelta]o f+Q D[f,Q]))


(* ::Section:: *)
(*Blocks for scalar external*)


(* ::Text:: *)
(*For the functions in this section we use the variables (Q,u,s) as in 1802.10537. *)
(*This meas that we focus only on the parity even blocks, for the parity odd one can use the functions in the next sections.*)


(* ::Subsection::Closed:: *)
(*Scalar internal*)


(* ::Text:: *)
(*Function which gives the blocks for scalar internal and external at q^orderQmax*)


ScalarIntExrtSolution[\[CapitalDelta]\[Phi]_,\[CapitalDelta]o_][orderQmax_]:=Module[{expTmp,tmpSol,tmp1,tmp2,tmpSolve,actCas},
expTmp[f_]:=Exponent[f,u^-1]/;Exponent[f,u^-1]>=0;
tmpSol=1;
Monitor[Do[
tmpSol+=Q^orderQ Sum[a[orderQ,i,j]u^i s^j,{i,0,orderQ},{j,0,orderQ}];
actCas=Q^-\[CapitalDelta]o MM[\[CapitalDelta]\[Phi],\[CapitalDelta]o,0][Q^\[CapitalDelta]o tmpSol]//Expand;
tmp1=Series[actCas,{Q,0,orderQ}]//Normal;
tmp2=CoefficientList[u^expTmp[tmp1] tmp1//Expand,{u,s,Q}];
tmpSolve=Solve[tmp2==0,Flatten@Table[a[orderQ,i,j],{i,0,orderQ},{j,0,orderQ}]]//Simplify;
tmpSol=(tmpSol/.Flatten@tmpSolve);

(*Some particular cases*)
If[orderQ==1&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==1,tmpSol=tmpSol/.{a[1,0,0]->2}];

,{orderQ,1,orderQmax}],orderQ];
tmpSol
]


(* ::Text:: *)
(*Function which increases the order of a scalar internal and external block from q^qPrev to q^orderQmax*)


ScalarIntExrtSolutionChekPoint[solPrevious_,qPrev_,\[CapitalDelta]\[Phi]_,\[CapitalDelta]o_][orderQmax_]:=Module[{expTmp,tmpSol,tmp1,tmp2,tmpSolve,actCas},
expTmp[f_]:=Exponent[f,u^-1]/;Exponent[f,u^-1]>=0;
tmpSol=solPrevious;
Monitor[Do[
tmpSol+=Q^orderQ Sum[a[i,j]u^i s^j,{i,0,orderQ},{j,0,orderQ}];
actCas=Q^-\[CapitalDelta]o MM[\[CapitalDelta]\[Phi],\[CapitalDelta]o,0][Q^\[CapitalDelta]o tmpSol]//Expand;
tmp1=Series[actCas,{Q,0,orderQ}]//Normal;
tmp2=CoefficientList[u^expTmp[tmp1] tmp1//Expand,{u,s,Q}];
tmpSolve=Solve[tmp2==0,Flatten@Table[a[i,j],{i,0,orderQ},{j,0,orderQ}]]//Simplify;
tmpSol=(tmpSol/.Flatten@tmpSolve);
,{orderQ,qPrev+1,orderQmax}],orderQ];
tmpSol
]


(* ::Subsection::Closed:: *)
(*Spinning internal*)


(* ::Text:: *)
(*Function which gives the blocks for spinning internal and scalar external at q^orderQmax. Here, we label the tensor structures by s[\[CapitalDelta]o, lo, i], where i \[Element] {1, ... , # of tens Strct}*)


(*The correct way to produce the blocks would be leaving all (\[CapitalDelta]\[Phi], \[CapitalDelta]o) general and then fix them after producing the blocks. This may be slow for higher powers, so it may better to fix them since the beginning to speed up the computation. However, this may not give the whole block and may be needed some extra fixing of coefficients.  Some of them are automatically done in (*Some particular cases*).*)


SpinningIntScalarExtSolution[\[CapitalDelta]\[Phi]_,\[CapitalDelta]o_,lo_][orderQmax_]:=Module[{expTmp,tmpSol,tmp1,tmp2,tmp3,tmpSolve,actCas,tmpVar,tmpVarFin,tmpRuleVar},
expTmp[f_]:=Exponent[f,u^-1]/;Exponent[f,u^-1]>=0;
tmpSol=0;
Monitor[Do[
tmpSol+=Q^orderQ Sum[a[orderQ,i,j]u^i s^j,{i,0,orderQ+lo},{j,0,orderQ+lo}];
tmpVar=Flatten@Table[a[orderQ,i,j],{i,0,orderQ+lo},{j,0,orderQ+lo}];
actCas=Q^-\[CapitalDelta]o MM[\[CapitalDelta]\[Phi],\[CapitalDelta]o,lo][Q^\[CapitalDelta]o tmpSol]//Expand;
tmp1=Series[actCas,{Q,0,orderQ}]//Normal;
tmp2=CoefficientList[u^expTmp[tmp1] tmp1//Expand,{u,s,Q}]//Flatten//DeleteDuplicates;
tmpSolve=Solve[tmp2==0,tmpVar]//Simplify//Quiet;
tmpSol=(tmpSol/.Flatten@tmpSolve);

(*Changing name to coefficients*)
If[orderQ==0,
tmp3=DeleteCases[(tmpVar/.tmpSolve//Expand)/.{Plus[A__,B__]:>List[A,B]}//Flatten//DeleteDuplicates,0];
tmpVarFin=tmp3/.{Num__ x__:>x/;NumberQ[Num]}//DeleteDuplicates;
tmpRuleVar=Table[Rule[tmpVarFin[[ii]],s[\[CapitalDelta]o,lo,ii]],{ii,Length@tmpVarFin}];
tmpSol=tmpSol/.tmpRuleVar;];

(*Some particular cases*)
If[orderQ==1&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==4&&lo==2,tmpSol=tmpSol/.{a[1,0,0]:>1/54 (141 s[4,2,1]+8 s[4,2,2])}];
(*Spin 3*)
If[orderQ==1&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==6&&lo==3,tmpSol=tmpSol/.{a[1,0,0]->1/270 (747 s[6,3,1]+8 s[6,3,2]),a[1,1,0]->(124425 s[6,3,1]-28760 s[6,3,2]-4608 s[6,3,3])/18900}];
If[orderQ==1&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==7&&lo==3,tmpSol=tmpSol/.{a[1,0,0]->1/135 (381 s[7,3,1]+2 s[7,3,2])}];
(*Spin 4*)
If[orderQ==2&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==11/2&&lo==4,tmpSol=tmpSol/.{a[2,0,0]->(2 (1245195 s[11/2,4,1]+113760 s[11/2,4,2]+8192 s[11/2,4,3]))/628425}];
If[orderQ==1&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==8&&lo==4,tmpSol=tmpSol/.{a[1,0,0]->1/756 (2141 s[8,4,1]+8 s[8,4,2]),a[1,1,0]->(61705 s[8,4,1]-9560 s[8,4,2]-512 s[8,4,3])/5880,a[1,2,0]->(574420 s[8,4,1]-131945 s[8,4,2]+18012 s[8,4,3]+7200 s[8,4,4])/48510}];
If[orderQ==1&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==10&&lo==4,tmpSol=tmpSol/.{a[1,0,0]->(5447 s[10,4,1]+8 s[10,4,2])/1890,a[1,1,0]->(1405565 s[10,4,1]-228280 s[10,4,2]-4608 s[10,4,3])/132300}];
If[orderQ==1&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==11&&lo==4,tmpSol=tmpSol/.{a[1,0,0]->1/675 (1955 s[11,4,1]+2 s[11,4,2])}];
(*Spin 5*)
If[orderQ==1&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==10&&lo==5,tmpSol=tmpSol/.{a[1,0,0]->(4647 s[10,5,1]+8 s[10,5,2])/1620,a[1,1,0]->(873075 s[10,5,1]-96040 s[10,5,2]-2304 s[10,5,3])/56700,a[1,2,0]->(3920070 s[10,5,1]-631015 s[10,5,2]+64278 s[10,5,3]+10800 s[10,5,4])/155925,a[1,3,0]->(895990095 s[10,5,1]-170592240 s[10,5,2]+31709448 s[10,5,3]-2837952 s[10,5,4]-3211264 s[10,5,5])/48648600}];
If[orderQ==1&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==13&&lo==5,tmpSol=tmpSol/.{a[1,0,0]->(6603 s[13,5,1]+4 s[13,5,2])/2268,a[1,1,0]->(4945605 s[13,5,1]-567140 s[13,5,2]-4608 s[13,5,3])/317520,a[1,2,0]->(44283855 s[13,5,1]-7342720 s[13,5,2]+870912 s[13,5,3]+43200 s[13,5,4])/1746360}];
If[orderQ==1&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==15&&lo==5,tmpSol=tmpSol/.{a[1,0,0]->(11061 s[15,5,1]+4 s[15,5,2])/3780,a[1,1,0]->(8276625 s[15,5,1]-962780 s[15,5,2]-4608 s[15,5,3])/529200}];
If[orderQ==2&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==13/2&&lo==5,tmpSol=tmpSol/.{a[2,0,0]->(2 (1311135 s[13/2,5,1]+81200 s[13/2,5,2]+4096 s[13/2,5,3]))/664125,a[2,1,0]->(2 (95700990 s[13/2,5,1]-517825 s[13/2,5,2]-5968896 s[13/2,5,3]-1843200 s[13/2,5,4]))/7305375,a[2,2,0]->(2 (2653630980 s[13/2,5,1]-191693775 s[13/2,5,2]-134053092 s[13/2,5,3]+82942080 s[13/2,5,4]+128450560 s[13/2,5,5]))/94969875}];
If[orderQ==2&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==15/2&&lo==5,tmpSol=tmpSol/.{a[2,0,0]->(2 (27235005 s[15/2,5,1]+377440 s[15/2,5,2]+8192 s[15/2,5,3]))/10749375,a[2,1,0]->(4 (908778255 s[15/2,5,1]-62622560 s[15/2,5,2]-14507008 s[15/2,5,3]-1843200 s[15/2,5,4]))/118243125}];
If[orderQ==2&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==8&&lo==5,tmpSol=tmpSol/.{a[2,0,0]->(5245695 s[8,5,1]+48440 s[8,5,2]+768 s[8,5,3])/1003275}];
(*Spin 6*)
If[orderQ==1&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==12&&lo==6,tmpSol=tmpSol/.{a[1,0,0]->(8589 s[12,6,1]+8 s[12,6,2])/2970,a[1,1,0]->(1107015 s[12,6,1]-90500 s[12,6,2]-1152 s[12,6,3])/51975,a[1,2,0]->(6048735 s[12,6,1]-718300 s[12,6,2]+56712 s[12,6,3]+4800 s[12,6,4])/127050,a[1,3,0]->(2171394225 s[12,6,1]-304028400 s[12,6,2]+42708060 s[12,6,3]-3063840 s[12,6,4]-1605632 s[12,6,5])/44594550,a[1,4,0]->(379117233495 s[12,6,1]-58272171600 s[12,6,2]+10684131984 s[12,6,3]-1854396960 s[12,6,4]-1812608 s[12,6,5]+362880000 s[12,6,6])/14404039650}];
If[orderQ==2&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==8&&lo==6,tmpSol=tmpSol/.{a[2,0,0]->(615195 s[8,6,1]+10840 s[8,6,2]+256 s[8,6,3])/128625,a[2,1,0]->(44628045 s[8,6,1]-1728760 s[8,6,2]-651264 s[8,6,3]-92160 s[8,6,4])/1131900,a[2,2,0]->(3091783695 s[8,6,1]-235340560 s[8,6,2]-33574464 s[8,6,3]+18670080 s[8,6,4]+12845056 s[8,6,5])/29429400,a[2,3,0]->(3830034142935 s[8,6,1]-391054649730 s[8,6,2]-8021040912 s[8,6,3]+28885484160 s[8,6,4]-6332612608 s[8,6,5]-28901376000 s[8,6,6])/28517088600}];
If[orderQ==2&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==19/2&&lo==6,tmpSol=tmpSol/.{a[2,0,0]->(2 (513227085 s[19/2,6,1]+2620960 s[19/2,6,2]+24576 s[19/2,6,3]))/191735775,a[2,1,0]->(4 (300825525 s[19/2,6,1]-18316738 s[19/2,6,2]-1379328 s[19/2,6,3]-73728 s[19/2,6,4]))/28121247,a[2,2,0]->(2 (506805919620 s[19/2,6,1]-47963406725 s[19/2,6,2]+518819424 s[19/2,6,3]+1161888000 s[19/2,6,4]+256901120 s[19/2,6,5]))/9139405275}];
If[orderQ==2&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==21/2&&lo==6,tmpSol=tmpSol/.{a[2,0,0]->(2 (221063745 s[21/2,6,1]+680240 s[21/2,6,2]+4096 s[21/2,6,3]))/80647875,a[2,1,0]->(2 (3863558160 s[21/2,6,1]-253289025 s[21/2,6,2]-10929152 s[21/2,6,3]-368640 s[21/2,6,4]))/177425325}];
If[orderQ==2&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==11&&lo==6,tmpSol=tmpSol/.{a[2,0,0]->(2 (7767480 s[11,6,1]+19325 s[11,6,2]+96 s[11,6,3]))/2811375}];
(*Spin 7*)
If[orderQ==1&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==14&&lo==7,tmpSol=tmpSol/.{a[1,0,0]->(14291 s[14,7,1]+8 s[14,7,2])/4914,a[1,1,0]->(1077727 s[14,7,1]-67880 s[14,7,2]-512 s[14,7,3])/38220,a[1,2,0]->(26112548 s[14,7,1]-2378365 s[14,7,2]+149412 s[14,7,3]+7200 s[14,7,4])/315315,a[1,3,0]->(4152191043 s[14,7,1]-444881580 s[14,7,2]+48822372 s[14,7,3]-2963584 s[14,7,4]-802816 s[14,7,5])/36891855,a[1,4,0]->(44031742755 s[14,7,1]-5174944500 s[14,7,2]+739424520 s[14,7,3]-101803520 s[14,7,4]+654976 s[14,7,5]+8064000 s[14,7,6])/529603074,a[1,5,0]->1/40602902340 (1443522415335 s[14,7,1]-179999172540 s[14,7,2]+30041250360 s[14,7,3]-5785061568 s[14,7,4]+763282048 s[14,7,5]+201384960 s[14,7,6]-356843520 s[14,7,7])}];
If[orderQ==2&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==19/2&&lo==7,tmpSol=tmpSol/.{a[2,0,0]->(2 (709976407 s[19/2,7,1]+5703520 s[19/2,7,2]+73728 s[19/2,7,3]))/278326125,a[2,1,0]->(2 (16344892949 s[19/2,7,1]-637120660 s[19/2,7,2]-87496704 s[19/2,7,3]-6635520 s[19/2,7,4]))/612317475,a[2,2,0]->(2 (79058197218 s[19/2,7,1]-5182193445 s[19/2,7,2]-242519328 s[19/2,7,3]+146089216 s[19/2,7,4]+51380224 s[19/2,7,5]))/884458575,a[2,3,0]->1/7713363232575 2 (1114969669689876 s[19/2,7,1]-92861228092215 s[19/2,7,2]+1504036078704 s[19/2,7,3]+2639998050352 s[19/2,7,4]-462062354432 s[19/2,7,5]-1040449536000 s[19/2,7,6]),a[2,4,0]->1/177407354349225 (47003990452184766 s[19/2,7,1]-4499220526843515 s[19/2,7,2]+271221419990664 s[19/2,7,3]+83731790727728 s[19/2,7,4]-47030630403328 s[19/2,7,5]-8154639360000 s[19/2,7,6]+69370380288000 s[19/2,7,7])}]
If[orderQ==2&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==23/2&&lo==7,tmpSol=tmpSol/.{a[2,0,0]->(2 (673472513 s[23/2,7,1]+1674080 s[23/2,7,2]+8192 s[23/2,7,3]))/244999755,a[2,1,0]->(2 (76259949689 s[23/2,7,1]-3868000840 s[23/2,7,2]-134846464 s[23/2,7,3]-3686400 s[23/2,7,4]))/2694997305,a[2,2,0]->(2 (652994504162 s[23/2,7,1]-49875168343 s[23/2,7,2]+1202580848 s[23/2,7,3]+472366336 s[23/2,7,4]+51380224 s[23/2,7,5]))/7006992993,a[2,3,0]->1/33948881051085 2 (5050520272381584 s[23/2,7,1]-463962918529275 s[23/2,7,2]+30195368367216 s[23/2,7,3]+2579411795792 s[23/2,7,4]-1453443776512 s[23/2,7,5]-578027520000 s[23/2,7,6])}];
If[orderQ==2&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==13&&lo==7,tmpSol=tmpSol/.{a[2,0,0]->(7723303 s[13,7,1]+10945 s[13,7,2]+32 s[13,7,3])/1378125,a[2,1,0]->(3484210961 s[13,7,1]-187331815 s[13,7,2]-3590656 s[13,7,3]-57600 s[13,7,4])/60637500,a[2,2,0]->(594438351507 s[13,7,1]-47223955350 s[13,7,2]+1788933568 s[13,7,3]+263070080 s[13,7,4]+16056320 s[13,7,5])/3153150000}];
If[orderQ==2&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==14&&lo==7,tmpSol=tmpSol/.{a[2,0,0]->(1010094281 s[14,7,1]+1052360 s[14,7,2]+2304 s[14,7,3])/178783605,a[2,1,0]->(454950697817 s[14,7,1]-25053491320 s[14,7,2]-348244992 s[14,7,3]-4147200 s[14,7,4])/7866478620}];
If[orderQ==2&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==29/2&&lo==7,tmpSol=tmpSol/.{a[2,0,0]->(2 (2358000099 s[29/2,7,1]+2136240 s[29/2,7,2]+4096 s[29/2,7,3]))/832001625}];
(*Spin 8*)
,{orderQ,0,orderQmax}],orderQ];
tmpSol
]


(* ::Text:: *)
(*Function which increases the order of a spinning internal and scalar external block from q^qPrev to q^orderQmax*)


SpinningIntScalarExtSolutionCheckPoint[solPrevious_,qPrev_,\[CapitalDelta]\[Phi]_,\[CapitalDelta]o_,lo_][orderQmax_]:=Module[{expTmp,tmpSol,tmp1,tmp2,tmpSolve,actCas,tmpVar},
expTmp[f_]:=Exponent[f,u^-1]/;Exponent[f,u^-1]>=0;
tmpSol=solPrevious;
Monitor[Do[
tmpSol+=Q^orderQ Sum[a[orderQ,i,j]u^i s^j,{i,0,orderQ+lo},{j,0,orderQ+lo}];
tmpVar=Flatten@Table[a[orderQ,i,j],{i,0,orderQ+lo},{j,0,orderQ+lo}];
actCas=Q^-\[CapitalDelta]o MM[\[CapitalDelta]\[Phi],\[CapitalDelta]o,lo][Q^\[CapitalDelta]o tmpSol]//Expand;
tmp1=Series[actCas,{Q,0,orderQ}]//Normal;
tmp2=CoefficientList[u^expTmp[tmp1] tmp1//Expand,{u,s,Q}];
tmpSolve=Solve[tmp2==0,tmpVar]//Simplify//Quiet;
tmpSol=(tmpSol/.Flatten@tmpSolve);

(*Special cases*)
If[orderQ==3&&\[CapitalDelta]\[Phi]==1&&\[CapitalDelta]o==7&&lo==6,tmpSol=tmpSol/.{a[3,0,0]->71690/8281}];

,{orderQ,qPrev+1,orderQmax}],orderQ];
tmpSol
]


(* ::Section:: *)
(*Blocks for spinning external*)


(* ::Subsection::Closed:: *)
(*Automatise solution at order Q^0*)


(* ::Text:: *)
(*Functions which gives the block for (\[CapitalDelta]\[Phi], l\[Phi], \[CapitalDelta]o, lo) at order q^0*)


SolutionQ0[\[CapitalDelta]\[Phi]_,l_,\[CapitalDelta]o_,lo_]:=Module[{var,ansatz,tmp1,tmp2,tmp3,tmp4,tmpCoeff,tmp5,tmp6,tmp7},
var=(Join[Flatten@Table[f0[m,n,k],{m,0,2lo+l},{n,-lo,lo},{k,0,l}],Flatten@Table[f1[m,n,k],{m,0,2lo+l},{n,-lo,lo},{k,0,l-1}],Flatten@Table[f2[m,n,k],{m,0,2lo+l-1},{n,-lo,lo},{k,0,l}],Flatten@Table[f3[m,n,k],{m,0,2lo+l-1},{n,-lo,lo},{k,0,l-1}]]//Flatten);
ansatz=(Sum[p^m t^n y^k f0[m,n,k],{m,0,2lo+l},{n,-lo,lo},{k,0,l}]+Sum[p^m t^n y^k f1[m,n,k]Sqrt[1+y^2],{m,0,2lo+l},{n,-lo,lo},{k,0,l-1}]+Sum[p^m t^n y^k f2[m,n,k]Sqrt[1-p^2],{m,0,2lo+l-1},{n,-lo,lo},{k,0,l}]+Sum[p^m t^n y^k f3[m,n,k]Sqrt[1+y^2] Sqrt[1-p^2],{m,0,2lo+l-1},{n,-lo,lo},{k,0,l-1}]);
tmp1=Series[lapFinQ\[CapitalDelta]o[\[CapitalDelta]\[Phi],l,\[CapitalDelta]o,lo][ansatz],{Q,0,0}]//Normal//Expand;
tmp2=(tmp1/.Plus[a__,b__]:>List[a,b]);
tmp3=PolynomialLCM@@((Denominator@#)&/@tmp2);
tmp4=(tmp2 tmp3)/.List[a__,b__]:>Plus[a,b]/.{Sqrt[1-p^2]->sqrP,Sqrt[1+y^2]->sqrY,(1-p^2)^(3/2)->(1-p^2)sqrP,(1+y^2)^(3/2)->(1+y^2)sqrY};
tmpCoeff=CoefficientList[tmp4,{t,p,sqrP,y,sqrY}]//Flatten//DeleteDuplicates;
tmp5=Solve[tmpCoeff==0,var]//Flatten//Quiet;
tmp6=DeleteCases[var/.tmp5/.{Plus[A__,B__]:>List[A,B]}//Flatten//DeleteDuplicates,0];
tmp7=tmp6/.{Num__ x__:>x/;NumberQ[Num]}//DeleteDuplicates;
{tmp7,ansatz/.tmp5}
]


(* ::Subsection::Closed:: *)
(*General order*)


(* ::Text:: *)
(*Functions which gives the block for (\[CapitalDelta]\[Phi], l\[Phi], \[CapitalDelta]o, lo) at order \!\(TraditionalForm\`*)
(*\*SuperscriptBox[\(q\), \(orderQmax\)]\) for one tensor structure corresponding to iSol \[Element] {1, ... # tensor structures}.*)


GeneralSolution[\[CapitalDelta]\[Phi]_,l_,\[CapitalDelta]o_,lo_][orderQmax_,iSol_]:=Module[{tmpQ,tmpcoeff,tmpSol,tPowersSet,pPowerMax,kmax,var,ansatz,tmp0,tmp1,tmp2,tmp3,tmp4,tmp5,tmp6},
tmpQ=SolutionQ0[\[CapitalDelta]\[Phi],l,\[CapitalDelta]o,lo]//Quiet;
If[iSol>Length@tmpQ[[1]],Print[ Length@tmpQ[[1]] "is the number of solutions"];Abort[]];
tmpcoeff=tmpQ[[1,iSol]];
tmp0=(#->0)&/@(Select[tmpQ[[1]],!MatchQ[#,tmpcoeff]&]);
tmpSol=(tmpQ[[2]]/.tmp0)/.tmpcoeff->1;
Monitor[Do[
tPowersSet=Table[i,{i,-lo-orderQ,lo+orderQ}];pPowerMax=2(lo+l+orderQ);kmax=l;
var=Join[Flatten@Table[f0[m,n,k],{m,0,pPowerMax},{n,tPowersSet},{k,0,kmax}],Flatten@Table[f1[m,n,k],{m,0,pPowerMax},{n,tPowersSet},{k,0,kmax-1}],Flatten@Table[f2[m,n,k],{m,0,pPowerMax-1},{n,tPowersSet},{k,0,kmax}],Flatten@Table[f3[m,n,k],{m,0,pPowerMax-1},{n,tPowersSet},{k,0,kmax-1}]]//Flatten;
ansatz=tmpSol+Q^orderQ (Sum[p^m t^n y^k f0[m,n,k],{m,0,pPowerMax},{n,tPowersSet},{k,0,kmax}]+Sum[p^m t^n y^k f1[m,n,k]Sqrt[1+y^2],{m,0,pPowerMax},{n,tPowersSet},{k,0,kmax-1}]+Sum[p^m t^n y^k f2[m,n,k]Sqrt[1-p^2],{m,0,pPowerMax-1},{n,tPowersSet},{k,0,kmax}]+Sum[p^m t^n y^k f3[m,n,k]Sqrt[1+y^2] Sqrt[1-p^2],{m,0,pPowerMax-1},{n,tPowersSet},{k,0,kmax-1}]);
tmp1=Series[lapFinQ\[CapitalDelta]o[\[CapitalDelta]\[Phi],l,\[CapitalDelta]o,lo][ansatz],{Q,0,orderQ}]//Normal//Expand;
tmp2=(tmp1/.Plus[a__,b__]:>List[a,b]);
tmp3=PolynomialLCM@@((Denominator@#)&/@tmp2);
tmp4=((tmp2/Q^orderQ tmp3)/.List[a__,b__]:>Plus[a,b])/.{Sqrt[1-p^2]->sqrP,Sqrt[1+y^2]->sqrY,(1-p^2)^(3/2)->(1-p^2)sqrP,(1+y^2)^(3/2)->(1+y^2)sqrY,(1-p^2)^(5/2)->(1-p^2)^2 sqrP,(1+y^2)^(5/2)->(1+y^2)^2 sqrY};
tmp5=CoefficientList[tmp4,{t,p,sqrP,y,sqrY}]//Flatten//DeleteDuplicates;
tmp6=Solve[tmp5==0,var]//Flatten;
tmpSol=ansatz/.tmp6;
,{orderQ,1,orderQmax}],orderQ];
tmpSol
]


(* ::Text:: *)
(*Functions which gives the block for (\[CapitalDelta]\[Phi], l\[Phi], \[CapitalDelta]o, lo) at order \!\(TraditionalForm\`*)
(*\*SuperscriptBox[\(q\), \(orderQmax\)]\)*)


GeneralSolution[\[CapitalDelta]\[Phi]_,l_,\[CapitalDelta]o_,lo_][orderQmax_]:=Module[{tmpQ,tmpcoeff,tmpnewcoeff,tmpSol,tmprule,tPowersSet,pPowerMax,kmax,var,ansatz,tmp0,tmp1,tmp2,tmp3,tmp4,tmp5,tmp6},
tmpQ=SolutionQ0[\[CapitalDelta]\[Phi],l,\[CapitalDelta]o,lo]//Quiet;
tmpcoeff=tmpQ[[1]];
tmpnewcoeff=((#/.a_[v__]:>a[v,q0]))&/@tmpcoeff;
tmprule=(#/.Plus[a_,b_]:>Rule[a,b])&/@(tmpcoeff+tmpnewcoeff);
tmpSol=tmpQ[[2]]/.tmprule;
Monitor[Do[
tPowersSet=Table[i,{i,-lo-orderQ,lo+orderQ}];pPowerMax=2(lo+l+orderQ);kmax=l;
var=Join[Flatten@Table[f0[m,n,k],{m,0,pPowerMax},{n,tPowersSet},{k,0,kmax}],Flatten@Table[f1[m,n,k],{m,0,pPowerMax},{n,tPowersSet},{k,0,kmax-1}],Flatten@Table[f2[m,n,k],{m,0,pPowerMax-1},{n,tPowersSet},{k,0,kmax}],Flatten@Table[f3[m,n,k],{m,0,pPowerMax-1},{n,tPowersSet},{k,0,kmax-1}]]//Flatten;
ansatz=tmpSol+Q^orderQ (Sum[p^m t^n y^k f0[m,n,k],{m,0,pPowerMax},{n,tPowersSet},{k,0,kmax}]+Sum[p^m t^n y^k f1[m,n,k]Sqrt[1+y^2],{m,0,pPowerMax},{n,tPowersSet},{k,0,kmax-1}]+Sum[p^m t^n y^k f2[m,n,k]Sqrt[1-p^2],{m,0,pPowerMax-1},{n,tPowersSet},{k,0,kmax}]+Sum[p^m t^n y^k f3[m,n,k]Sqrt[1+y^2] Sqrt[1-p^2],{m,0,pPowerMax-1},{n,tPowersSet},{k,0,kmax-1}]);
tmp1=Series[lapFinQ\[CapitalDelta]o[\[CapitalDelta]\[Phi],l,\[CapitalDelta]o,lo][ansatz],{Q,0,orderQ}]//Normal//Expand;
tmp2=(tmp1/.Plus[a__,b__]:>List[a,b]);
tmp3=PolynomialLCM@@((Denominator@#)&/@tmp2);
tmp4=((tmp2/Q^orderQ tmp3)/.List[a__,b__]:>Plus[a,b])/.{Sqrt[1-p^2]->sqrP,Sqrt[1+y^2]->sqrY,(1-p^2)^(3/2)->(1-p^2)sqrP,(1+y^2)^(3/2)->(1+y^2)sqrY,(1-p^2)^(5/2)->(1-p^2)^2 sqrP,(1+y^2)^(5/2)->(1+y^2)^2 sqrY};
tmp5=CoefficientList[tmp4,{t,p,sqrP,y,sqrY}]//Flatten//DeleteDuplicates;
tmp6=Solve[tmp5==0,var]//Flatten;
tmpSol=(ansatz/.tmp6);
,{orderQ,1,orderQmax}],orderQ];
tmpSol
]


(* ::Subsection::Closed:: *)
(*Imposing conservations of externals*)


(* ::Text:: *)
(*Functions which gives the conserved block for (l\[Phi]+1, l\[Phi], \[CapitalDelta]o, lo) at order \!\(TraditionalForm\`*)
(*\*SuperscriptBox[\(q\), \(0\)]\) .*)


(*We get the previous block at order q^0 and then we impose conservation of the external dvQtyp[block]=0 which fixes some of the tensor structures.*)


SolutionQ0Cons[l\[Phi]_,\[CapitalDelta]o_,lo_]:=Module[{soltmp,\[CapitalDelta]\[Phi],tmpSolve,tmpSolution,tmp1,tmp2,tmp3,tmp4,tmp5,tmp6,tmp7,sqrP,sqrY},
soltmp=SolutionQ0[\[CapitalDelta]\[Phi],l\[Phi],\[CapitalDelta]o,lo]//Quiet;
tmp1=Series[dvQtyp[l\[Phi]+1,l\[Phi]][(soltmp[[2]]/.\[CapitalDelta]\[Phi]->l\[Phi]+1)],{Q,0,0}]//Normal//Expand;
tmp2=(tmp1/.Plus[a__,b__]:>List[a,b]);
tmp3=PolynomialLCM@@((Denominator@#)&/@tmp2);
tmp4=((tmp2 tmp3)/.{Sqrt[1-p^2]->sqrP,Sqrt[1+y^2]->sqrY,(1-p^2)^(3/2)->(1-p^2)sqrP,(1+y^2)^(3/2)->(1+y^2)sqrY,(1-p^2)^(5/2)->(1-p^2)^2 sqrP,(1+y^2)^(5/2)->(1+y^2)^2 sqrY})/.List[a__,b__]:>Plus[a,b];
tmp5=CoefficientList[tmp4,{t,p,sqrP,y,sqrY}]//Flatten//DeleteDuplicates;
tmpSolve=Solve[tmp5==0,soltmp[[1]]]//Flatten//Quiet;
tmpSolution=soltmp[[2]]/.tmpSolve;
tmp6=DeleteCases[soltmp[[1]]/.tmpSolve/.{Plus[A__,B__]:>List[A,B]}//Flatten//DeleteDuplicates,0];
tmp7=tmp6/.{Num__ x__:>x/;NumberQ[Num]}//DeleteDuplicates;
{tmp7,tmpSolution}
]


(* ::Text:: *)
(*Functions which gives the conserved block for (l\[Phi]+1, l\[Phi], \[CapitalDelta]o, lo) at order \!\(TraditionalForm\`*)
(*\*SuperscriptBox[\(q\), \(orderQmax\)]\) for one tensor structure corresponding to iSol \[Element] {1, ... # tensor structures (after conservation)}.*)


GeneralSolutionCons[l_,\[CapitalDelta]o_,lo_][orderQmax_,iSol_]:=Module[{\[CapitalDelta]\[Phi],tmpQ,tmpcoeff,tmpSol,tPowersSet,pPowerMax,kmax,var,ansatz,tmp0,tmp1,tmp2,tmp3,tmp4,tmp5,tmp6},
tmpQ=SolutionQ0Cons[l,\[CapitalDelta]o,lo]//Simplify//Quiet;
If[iSol>Length@tmpQ[[1]],Print[ Length@tmpQ[[1]] "is the number of solutions"];Abort[]];
tmpcoeff=tmpQ[[1,iSol]];
tmp0=(#->0)&/@(Select[tmpQ[[1]],!MatchQ[#,tmpcoeff]&]);
tmpSol=(tmpQ[[2]]/.tmp0)/.tmpcoeff->1;
Monitor[Do[
tPowersSet=Table[i,{i,-lo-orderQ,lo+orderQ}];pPowerMax=2(lo+l+orderQ);kmax=l;
var=Join[Flatten@Table[f0[m,n,k],{m,0,pPowerMax},{n,tPowersSet},{k,0,kmax}],Flatten@Table[f1[m,n,k],{m,0,pPowerMax},{n,tPowersSet},{k,0,kmax-1}],Flatten@Table[f2[m,n,k],{m,0,pPowerMax-1},{n,tPowersSet},{k,0,kmax}],Flatten@Table[f3[m,n,k],{m,0,pPowerMax-1},{n,tPowersSet},{k,0,kmax-1}]]//Flatten;
ansatz=tmpSol+Q^orderQ (Sum[p^m t^n y^k f0[m,n,k],{m,0,pPowerMax},{n,tPowersSet},{k,0,kmax}]+Sum[p^m t^n y^k f1[m,n,k]Sqrt[1+y^2],{m,0,pPowerMax},{n,tPowersSet},{k,0,kmax-1}]+Sum[p^m t^n y^k f2[m,n,k]Sqrt[1-p^2],{m,0,pPowerMax-1},{n,tPowersSet},{k,0,kmax}]+Sum[p^m t^n y^k f3[m,n,k]Sqrt[1+y^2] Sqrt[1-p^2],{m,0,pPowerMax-1},{n,tPowersSet},{k,0,kmax-1}]);
tmp1=Series[lapFinQ\[CapitalDelta]o[\[CapitalDelta]\[Phi],l,\[CapitalDelta]o,lo][ansatz],{Q,0,orderQ}]//Normal//Expand;
tmp2=(tmp1/.Plus[a__,b__]:>List[a,b]);
tmp3=PolynomialLCM@@((Denominator@#)&/@tmp2);
tmp4=((tmp2/Q^orderQ tmp3)/.List[a__,b__]:>Plus[a,b])/.{Sqrt[1-p^2]->sqrP,Sqrt[1+y^2]->sqrY,(1-p^2)^(3/2)->(1-p^2)sqrP,(1+y^2)^(3/2)->(1+y^2)sqrY,(1-p^2)^(5/2)->(1-p^2)^2 sqrP,(1+y^2)^(5/2)->(1+y^2)^2 sqrY};
tmp5=CoefficientList[tmp4,{t,p,sqrP,y,sqrY}]//Flatten//DeleteDuplicates;
tmp6=Solve[tmp5==0,var]//Flatten;
tmpSol=(ansatz/.tmp6)/.\[CapitalDelta]\[Phi]->l+1;
,{orderQ,1,orderQmax}],orderQ];
tmpSol
]


(* ::Text:: *)
(*Functions which gives the block for (l\[Phi]+1, l\[Phi], \[CapitalDelta]o, lo) at order \!\(TraditionalForm\`*)
(*\*SuperscriptBox[\(q\), \(orderQmax\)]\)*)


GeneralSolutionCons[l_,\[CapitalDelta]o_,lo_][orderQmax_]:=Module[{\[CapitalDelta]\[Phi],tmpQ,tmpcoeff,tmpnewcoeff,tmpSol,tmprule,tPowersSet,pPowerMax,kmax,var,ansatz,tmp0,tmp1,tmp2,tmp3,tmp4,tmp5,tmp6},
tmpQ=SolutionQ0Cons[l,\[CapitalDelta]o,lo]//Simplify//Quiet;
tmpcoeff=tmpQ[[1]];
tmpnewcoeff=((#/.a_[v__]:>a[v,q0]))&/@tmpcoeff;
tmprule=(#/.Plus[a_,b_]:>Rule[a,b])&/@(tmpcoeff+tmpnewcoeff);
tmpSol=tmpQ[[2]]/.tmprule;
Monitor[Do[
tPowersSet=Table[i,{i,-lo-orderQ,lo+orderQ}];pPowerMax=2(lo+l+orderQ);kmax=l;
var=Join[Flatten@Table[f0[m,n,k],{m,0,pPowerMax},{n,tPowersSet},{k,0,kmax}],Flatten@Table[f1[m,n,k],{m,0,pPowerMax},{n,tPowersSet},{k,0,kmax-1}],Flatten@Table[f2[m,n,k],{m,0,pPowerMax-1},{n,tPowersSet},{k,0,kmax}],Flatten@Table[f3[m,n,k],{m,0,pPowerMax-1},{n,tPowersSet},{k,0,kmax-1}]]//Flatten;
ansatz=tmpSol+Q^orderQ (Sum[p^m t^n y^k f0[m,n,k],{m,0,pPowerMax},{n,tPowersSet},{k,0,kmax}]+Sum[p^m t^n y^k f1[m,n,k]Sqrt[1+y^2],{m,0,pPowerMax},{n,tPowersSet},{k,0,kmax-1}]+Sum[p^m t^n y^k f2[m,n,k]Sqrt[1-p^2],{m,0,pPowerMax-1},{n,tPowersSet},{k,0,kmax}]+Sum[p^m t^n y^k f3[m,n,k]Sqrt[1+y^2] Sqrt[1-p^2],{m,0,pPowerMax-1},{n,tPowersSet},{k,0,kmax-1}]);
tmp1=Series[lapFinQ\[CapitalDelta]o[\[CapitalDelta]\[Phi],l,\[CapitalDelta]o,lo][ansatz],{Q,0,orderQ}]//Normal//Expand;
tmp2=(tmp1/.Plus[a__,b__]:>List[a,b]);
tmp3=PolynomialLCM@@((Denominator@#)&/@tmp2);
tmp4=((tmp2/Q^orderQ tmp3)/.List[a__,b__]:>Plus[a,b])/.{Sqrt[1-p^2]->sqrP,Sqrt[1+y^2]->sqrY,(1-p^2)^(3/2)->(1-p^2)sqrP,(1+y^2)^(3/2)->(1+y^2)sqrY,(1-p^2)^(5/2)->(1-p^2)^2 sqrP,(1+y^2)^(5/2)->(1+y^2)^2 sqrY};
tmp5=CoefficientList[tmp4,{t,p,sqrP,y,sqrY}]//Flatten//DeleteDuplicates;
tmp6=Solve[tmp5==0,var]//Flatten;
tmpSol=(ansatz/.tmp6)/.\[CapitalDelta]\[Phi]->l+1;
,{orderQ,1,orderQmax}],orderQ];
tmpSol
]


(* ::Subsection::Closed:: *)
(*Exact blocks for \[Mu] = 0*)


(* ::Text:: *)
(*Exact  block (no expansion in Q) for 0 chemical potential (t = 0), which also implies no dependence over spacetime invariant p.*)


(* ::Text:: *)
(*Block for scalar internal and external*)


BlockInt0Ext0[Q_]:=(1-Q)^(-2\[CapitalDelta]o) HypergeometricPFQ[{\[CapitalDelta]o-1,\[CapitalDelta]o-\[CapitalDelta]\[Phi]/2,\[CapitalDelta]o+\[CapitalDelta]\[Phi]/2-3/2},{\[CapitalDelta]o,2\[CapitalDelta]o-2},(-4Q)/(1-Q)^2];


(* ::Text:: *)
(*Block  for external spin 1 and internal scalar*)


BlockInt0Ext1[Q_]:=I (1-Q)^(-1-2 \[CapitalDelta]o) (1+Q) (Sqrt[1-p^2]+p y) (2 (-1+\[CapitalDelta]o) HypergeometricPFQ[{\[CapitalDelta]o-\[CapitalDelta]\[Phi]/2,-(3/2)+\[CapitalDelta]o+\[CapitalDelta]\[Phi]/2},{-2+2 \[CapitalDelta]o},-((4 Q)/(-1+Q)^2)]-(-2+\[CapitalDelta]\[Phi]) HypergeometricPFQ[{-1+\[CapitalDelta]o,\[CapitalDelta]o-\[CapitalDelta]\[Phi]/2,-(3/2)+\[CapitalDelta]o+\[CapitalDelta]\[Phi]/2},{\[CapitalDelta]o,2 \[CapitalDelta]o-2},-((4 Q)/(-1+Q)^2)])
